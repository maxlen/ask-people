alias l='ls -CF'
alias la='ls -A'
alias ll='ls -alF'
alias ls='ls --color=auto'

export PS1='\e[0;32m\u@\H\e[m(ask-people-postgres):\e[0;2m\w\e[m$ '